# Digitalia Chat

Digitalia chat challenge

[![Built with Cookiecutter Django](https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg?logo=cookiecutter)](https://github.com/cookiecutter/cookiecutter-django/)
[![Black code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

License: BSD

## Basic Set Up

This project uses Docker, so it's needed to have the following versions installed —at least—:

- Docker 17.05+.
- Docker Compose 1.17+

### Run the project

To run the project, first set the environment variables, there's a folder `.envs_sample`, just copy/move that to
`.envs` folder:

`$ cp -r .envs_sample .envs`

There are some basic variables already set with default values (no need to change them as it's a local development
project, but keep in mind that when deploying to production environment, many of those values should be changed),

Go to the project's root folder and run it using the `local.yml` docker-compose file:

`$ docker compose -f local.yml up django`

the first time it will build the corresponding containers, install the dependencies of the project, when Docker
finishes doing its magic, you'll see in the terminal the following lines:

```shell
System check identified no issues (0 silenced).

Django version 4.2.6, using settings 'config.settings.local'
Starting ASGI/Daphne version 4.0.0 development server at http://0.0.0.0:8000/
Quit the server with CONTROL-C.

```

That means that the project is ready to run, you can go to your browser at http://localhost:8000/, and you'll see
the main page of the project!

### Setting Up Your Users

- To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it, you'll see a
  "Verify Your E-mail Address" page. Go to your console to see a simulated email verification message. Copy the link
  into your browser. Now the user's email should be verified and ready to go.


## Another Considerations

### Sentry

Sentry is an error logging aggregator service. You can sign up for a free account at <https://sentry.io/signup/?code=cookiecutter> or download and host it yourself.
The system is set up with reasonable defaults, including 404 logging and integration with the WSGI application.

You must set the DSN url in production.

## Deployment

The following details how to deploy this application.

### Docker

See detailed [cookiecutter-django Docker documentation](http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html).
