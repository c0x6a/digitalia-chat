"""
Test chat app views
"""
from django.test import TestCase
from django.urls import reverse

from digitalia_chat.chat.models import ChatRoom
from digitalia_chat.users.models import User


class TestChatRoomViews(TestCase):
    """Chat room views tests"""

    fixtures = ("users.json",)

    def setUp(self) -> None:
        self.chat_room_name = "chat_room"
        self.chat_room = ChatRoom.objects.create(name=self.chat_room_name)
        self.user = User.objects.get(username="pepito")  # data from fixtures file

    def test_user_creates_chat_room(self):
        self.client.force_login(user=self.user)

        room_name = "test_chat_room"
        data = {"room-name": room_name}
        url = reverse("chat:get-create-room")
        response = self.client.post(url, data)

        expected_url = reverse("chat:room", kwargs={"room_name": room_name})

        assert response.status_code == 302
        assert response.url == expected_url

    def test_user_access_chat_room(self):
        self.client.force_login(user=self.user)

        url = reverse("chat:room", kwargs={"room_name": self.chat_room.name})
        response = self.client.get(url)

        assert response.status_code == 200
        assert "chat/room.html" in response.template_name

    def test_access_chat_room_no_user(self):
        url = reverse("chat:room", kwargs={"room_name": self.chat_room.name})
        response = self.client.get(url)

        login_url = reverse("account_login")
        login_url = f"{login_url}?next=/chat/{self.chat_room.name}/"

        assert response.status_code == 302
        assert response.url == login_url

    def test_list_chat_rooms(self):
        self.client.force_login(user=self.user)

        url = reverse("chat:index")
        response = self.client.get(url)

        assert response.status_code == 200
        assert response.context_data.get("rooms").get(name=self.chat_room_name) == self.chat_room

    def test_list_chat_rooms_no_user(self):
        url = reverse("chat:index")
        response = self.client.get(url)

        login_url = reverse("account_login")
        login_url = f"{login_url}?next=/"

        assert response.status_code == 302
        assert response.url == login_url
