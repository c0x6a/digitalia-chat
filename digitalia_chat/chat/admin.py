"""
Admin models for chat app
"""
from django.contrib import admin

from digitalia_chat.chat.models import ChatRoom


@admin.register(ChatRoom)
class ChatRoomModelAdmin(admin.ModelAdmin):
    list_display = ("name",)
