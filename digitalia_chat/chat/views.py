"""
Views definition for chat app
"""
from typing import Any

from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.views.generic.base import TemplateView, View

from digitalia_chat.chat.models import ChatRoom


class IndexTemplateView(LoginRequiredMixin, TemplateView):
    """Index view"""

    template_name = "chat/chats_index.html"

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        rooms = ChatRoom.objects.all()
        context.update({"rooms": rooms})
        return context


index_template_view = IndexTemplateView.as_view()


class RoomGetCreateView(LoginRequiredMixin, View):
    """Get or create a chat room"""

    def get(self, request, *args, **kwargs):
        return redirect("chat:index")

    def post(self, request, *args, **kwargs):
        room_name = request.POST.get("room-name")
        room, _ = ChatRoom.objects.get_or_create(name=room_name)
        return redirect("chat:room", **{"room_name": room.name})


room_get_or_create_view = RoomGetCreateView.as_view()


class RoomTemplateView(LoginRequiredMixin, TemplateView):
    """Room view"""

    template_name = "chat/room.html"

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        room_name = kwargs.get("room_name")
        context.update({"room_name": room_name})
        return context


room_template_view = RoomTemplateView.as_view()
