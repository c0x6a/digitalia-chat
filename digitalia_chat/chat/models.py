"""
Models definition for chat app
"""
from django.db import models


class ChatRoom(models.Model):
    name = models.CharField(max_length=32, unique=True)

    def __str__(self):
        return self.name
