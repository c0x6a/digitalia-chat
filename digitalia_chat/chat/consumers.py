"""
Consumers definition for chat app
"""
import json

import redis
from channels.generic.websocket import AsyncWebsocketConsumer
from django.conf import settings


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope["url_route"]["kwargs"]["room_name"]
        self.room_group_name = f"chat_{self.room_name}"

        self.redis = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT)
        try:
            messages = self.redis.lrange(f"{self.room_group_name}:messages", 0, -1)
        except Exception:
            messages = []

        await self.channel_layer.group_add(self.room_group_name, self.channel_name)

        await self.accept()

        user = self.scope["user"]

        for message in messages:
            await self.channel_layer.group_send(
                self.room_group_name, {"type": "history_message", "message": message.decode(), "user": str(user)}
            )

        join_message = f"{user} has joined the room."
        await self.channel_layer.group_send(self.room_group_name, {"type": "service_message", "message": join_message})

    async def disconnect(self, close_code):
        user = str(self.scope["user"])
        message = f"{user} left the room."
        await self.channel_layer.group_send(self.room_group_name, {"type": "service_message", "message": message})
        await self.channel_layer.group_discard(self.room_group_name, self.channel_name)

    async def receive(self, text_data, **kwargs):
        text_data_json = json.loads(text_data)
        message = text_data_json["message"]
        username = text_data_json["user"]

        message = f"{username}: {message}"

        await self.channel_layer.group_send(self.room_group_name, {"type": "chat_message", "message": message})
        self.redis.rpush(f"{self.room_group_name}:messages", message)

    async def chat_message(self, event):
        message = event["message"]

        await self.send(text_data=json.dumps({"message": message}))

    async def service_message(self, event):
        """Service messages, when a user joins or leaves the room"""
        message = event.get("message")
        await self.send(text_data=json.dumps({"message": f"\t------ {message} ------"}))

    async def history_message(self, event):
        """Service messages, when a user joins or leaves the room"""
        message = event.get("message")
        user = event.get("user")
        await self.send(text_data=json.dumps({"message": message, "is_history": True, "user": str(user)}))
