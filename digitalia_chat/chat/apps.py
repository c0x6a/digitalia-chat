"""
Views definition for chat app
"""
from django.apps import AppConfig


class ChatConfig(AppConfig):
    """Chat app configuration"""

    name = "digitalia_chat.chat"
