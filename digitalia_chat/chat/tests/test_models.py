"""
Test chat app models
"""
from django.test import TestCase

from digitalia_chat.chat.models import ChatRoom


class TestChatRoomModel(TestCase):
    """Chat room model tests"""

    def test_create_model(self):
        room_name = "test_room"
        test_chat_room = ChatRoom.objects.create(name=room_name)

        assert str(test_chat_room) == room_name
