"""
URLs configuration for chat app
"""
from django.urls import path

from digitalia_chat.chat import views

app_name = "chat"

urlpatterns = [
    path("", views.index_template_view, name="index"),
    path("chat/", views.room_get_or_create_view, name="get-create-room"),
    path("chat/<str:room_name>/", views.room_template_view, name="room"),
]
